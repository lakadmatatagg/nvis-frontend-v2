export const GlobalConstant = {
    LABEL_COMPLETED: 'Siap',
    LABEL_TASK_COMPLETED: 'Tugasan Selesai',
    LABEL_ON_TRACK: 'Dalam Tindakan',
    LABEL_AT_RISK: '3 Hari Sebelum Piagam',
    LABEL_OVERDUE: 'Melepasi Piagam',
    LABEL_OPEN: 'Belum Siap',

    LABEL_COUNT_OPEN: 'Tugasan Belum Diselesaikan Semasa',
    LABEL_GENERATED_TODAY: 'Tugasan Baru Dijana Hari Ini',
    LABEL_DUE_TODAY: 'Tugasan Yang Perlu Diselesaikan Hari Ini',
    LABEL_COMPLETED_TODAY: 'Tugasan Diselesaikan Hari Ini',

    DESC_COMPLETED: 'Tugas yang telah siap',
    DESC_ON_TRACK: 'Tugas yang sedang dalam tindakan',
    DESC_AT_RISK: 'Tugas yang 3 hari sebelum piagam',
    DESC_OVERDUE: 'Tugas yang telah melepasi piagam',

    COLOR_COMPLETED: '#00b77c',
    COLOR_ON_TRACK: '#00c5c5',
    COLOR_AT_RISK: '#CF6C02',
    COLOR_OVERDUE: '#ff1a30'
};
