import {Component, Input} from '@angular/core';
import {NvisCardComponent} from "../nvis-card/nvis-card.component";
import {GlobalConstant} from "../../constant/global.constant";

@Component({
  selector: 'app-stats-count',
  standalone: true,
    imports: [
        NvisCardComponent
    ],
  templateUrl: './app-stats-count.component.html',
  styleUrl: './app-stats-count.component.scss'
})
export class AppStatsCountComponent {
    @Input() type: string = 'countOpen';
    @Input() countOpen: number = 0;
    @Input() countGeneratedToday: number = 0;
    @Input() countDueToday: number = 0;
    @Input() countCompletedToday: number = 0;

    get statTitle() {
        if (this.type == 'countOpen') {
            return GlobalConstant.LABEL_COUNT_OPEN;
        } else if (this.type == 'countGeneratedToday') {
            return GlobalConstant.LABEL_GENERATED_TODAY;
        } else if (this.type == 'countDueToday') {
            return GlobalConstant.LABEL_DUE_TODAY;
        } else if (this.type == 'countCompletedToday') {
            return GlobalConstant.LABEL_COMPLETED_TODAY;
        } else {
            return '';
        }
    }

    get count() {
        if (this.type == 'countOpen') {
            return this.countOpen;
        } else if (this.type == 'countGeneratedToday') {
            return this.countGeneratedToday;
        } else if (this.type == 'countDueToday') {
            return this.countDueToday;
        } else if (this.type == 'countCompletedToday') {
            return this.countCompletedToday;
        } else {
            return 0;
        }
    }
}
