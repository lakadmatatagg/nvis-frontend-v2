import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppStatsCountComponent } from './app-stats-count.component';

describe('AppStatsCountComponent', () => {
  let component: AppStatsCountComponent;
  let fixture: ComponentFixture<AppStatsCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [AppStatsCountComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(AppStatsCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
