import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NvisCardComponent } from './nvis-card.component';

describe('NvisCardComponent', () => {
  let component: NvisCardComponent;
  let fixture: ComponentFixture<NvisCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [NvisCardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(NvisCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
