import { Component } from '@angular/core';
import {EChartsOption} from "echarts";
import {GlobalConstant} from "../../constant/global.constant";
import {NgxEchartsDirective, provideEcharts} from "ngx-echarts";

@Component({
  selector: 'app-stats',
  standalone: true,
    imports: [
        NgxEchartsDirective
    ],
  templateUrl: './app-stats.component.html',
  styleUrl: './app-stats.component.scss',
  providers: [
      provideEcharts()
  ]
})
export class AppStatsComponent {
    countCompleted: number = 0;
    countOnTrack: number = 0;
    countOverdue: number = 0;
    countAtRisk: number = 0;

    mergeOptions = {};

    optionsPie: EChartsOption = {
        tooltip: {
            trigger: 'item',
            // formatter: '{b}: {c} ({d}%)'
            formatter: (params) => {
                return `${params.name}: ${params.data.value} (${params.percent}%)<br />
                ${params.data.name1}`;
            }
        },
        legend: {
            orient: 'vertical',
            left: 'left',
            top: 'auto',
            padding: [50, 10, 10, 10],
            data: [GlobalConstant.LABEL_TASK_COMPLETED, GlobalConstant.LABEL_ON_TRACK, GlobalConstant.LABEL_AT_RISK, GlobalConstant.LABEL_OVERDUE]
        },
        grid: {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
            containLabel: true
        },
        series: [
            {
                type: 'pie',
                radius: '78%',
                center: ['70%', '54%'],
                label: {
                    formatter: '{c}: {b}'
                },
                labelLine: {
                    show: true,
                    length: 5
                },
                data: [
                    { value: this.countCompleted, name: GlobalConstant.LABEL_TASK_COMPLETED, itemStyle: { color: GlobalConstant.COLOR_COMPLETED } },
                    { value: this.countOnTrack, name: GlobalConstant.LABEL_ON_TRACK, itemStyle: { color: GlobalConstant.COLOR_ON_TRACK } },
                    { value: this.countAtRisk, name: GlobalConstant.LABEL_AT_RISK, itemStyle: { color: GlobalConstant.COLOR_AT_RISK } },
                    { value: this.countOverdue, name: GlobalConstant.LABEL_OVERDUE, itemStyle: { color: GlobalConstant.COLOR_OVERDUE } }
                ],
                emphasis: {
                    itemStyle: {
                        shadowBlur: 4,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.15)'
                    }
                }
            }
        ]
    };
}
