import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PengurusanKesComponent } from './pengurusan-kes.component';

describe('PengurusanKesComponent', () => {
  let component: PengurusanKesComponent;
  let fixture: ComponentFixture<PengurusanKesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PengurusanKesComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PengurusanKesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
