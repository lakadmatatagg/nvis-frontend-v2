import { Component } from '@angular/core';
import {LogoComponent} from "../../_shared/components/logo/logo.component";
import {NvisCardComponent} from "../../_shared/components/nvis-card/nvis-card.component";
import {AppStatsComponent} from "../../_shared/components/app-stats/app-stats.component";
import {AppStatsCountComponent} from "../../_shared/components/app-stats-count/app-stats-count.component";

@Component({
  selector: 'app-pengurusan-kes',
  standalone: true,
    imports: [
        LogoComponent,
        NvisCardComponent,
        AppStatsComponent,
        AppStatsCountComponent
    ],
  templateUrl: './pengurusan-kes.component.html',
  styleUrl: './pengurusan-kes.component.scss'
})
export class PengurusanKesComponent {

}
